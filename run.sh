opts=''
while getopts "e:" opt; do
    case $opt in
        e) entrypoint=" --entrypoint $OPTARG ";;
    esac
done
shift $((OPTIND-1))

export DISPLAY=:0
xhost +
opts+=" -e DISPLAY=$DISPLAY "
opts+=" -v /tmp/.X11-unix:/tmp/.X11-unix "
opts+=" -v $PWD:/data "
opts+=" -w /data "
opts+=" --rm -it "
docker run $entrypoint $opts weka $*
