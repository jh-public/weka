FROM ubuntu:20.04

ENV STABLE=https://udomain.dl.sourceforge.net/project/weka/weka-3-8/3.8.5/weka-3-8-5-azul-zulu-linux.zip
ENV DEV=https://jaist.dl.sourceforge.net/project/weka/weka-3-9/3.9.5/weka-3-9-5-azul-zulu-linux.zip
ARG ver=developer

RUN date \
    && echo -e '\033[31m'Building ${ver} version... '\033[0m' \
    && apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
       apt-get install -y --no-install-recommends \
        build-essential \
        cmake \
        default-jre \
        firefox \
        git \
        unzip \
        wget \
    && echo

RUN echo Installing Weka \
    && if test "${ver}" = "developer"; then \
          export url=${DEV}; \
       else \
          export url=${STABLE}; \
       fi \
    && wget ${url} -O weka.zip \
    && unzip weka.zip \
    && mv $( ls -1d /weka-* | grep -v .zip | tail -1 ) /weka \
    && mkdir -p /root/wekafiles/systemDialogs \
    && touch    /root/wekafiles/systemDialogs/weka.gui.GUIChooser.HowToFindPackageManager \
    && ln -s /weka /root/ \
    && rm -f weka.zip \
    && echo

RUN echo Installing numeric datasets \
    && wget https://jaist.dl.sourceforge.net/project/weka/datasets/datasets-numeric/datasets-numeric.jar \
    && unzip datasets-numeric.jar -d /weka/data/ \
    && rm datasets-numeric.jar \
    && echo

RUN echo Installing XgBoost \
    && git clone --recursive https://github.com/dmlc/xgboost \
    && cd xgboost \
    && mkdir build \
    && cd build \
    && cmake .. \
    && make -j4 \
    && cd / \
    && echo

RUN echo Installing WekaDeeplearning4j \
    && wget https://github.com/SigDelta/weka-xgboost/releases/download/0.1.0/xgboost-0.1.0.zip -O xgboost.zip \
    && java -cp /weka/weka.jar weka.core.WekaPackageManager -install-package xgboost.zip \
    && rm xgboost.zip \
    \
    && wget https://github.com/Waikato/wekaDeeplearning4j/releases/download/v1.7.1/wekaDeeplearning4j-1.7.1.zip  -O wekaDeeplearning4j.zip \
    && java -cp /weka/weka.jar weka.core.WekaPackageManager -install-package wekaDeeplearning4j.zip \
    && rm wekaDeeplearning4j.zip

WORKDIR /weka

ENTRYPOINT [ "/usr/bin/java", "-jar", "/weka/weka.jar" ]
