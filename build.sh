#!/usr/bin/env bash

ver=developer
while getopts 'ds' opt; do
  case $opt in
    d) ver=developer;;
    s) ver=stable;;
  esac
done
shift $((OPTIND-1))

time docker build  --build-arg ver=$ver $* -t huanjason/weka .
docker tag huanjason/weka huanjason/weka:${ver}

