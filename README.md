# Weka 3.9 with XGBoost and WekaDeeplearning4j

## Usage

    xhost +
    docker run \
          -e DISPLAY \
          -v /tmp/.X11-unix:/tmp/.X11-unix \
          -v $HOME/.Xauthority:/root/.Xauthority \
          -v $HOME/wekafiles:/root/wekafiles \
          -v $PWD:/data \
          -w /data \
          --net host \
          huanjason/weka

### OSX
[How to show X11 windows with Docker on Mac
](https://medium.com/@mreichelt/how-to-show-x11-windows-within-docker-on-mac-50759f4b65cb)

### Windows
[Installing/Configuring PuTTy and Xming
](http://www.geo.mtu.edu/geoschem/docs/putty_install.html)

